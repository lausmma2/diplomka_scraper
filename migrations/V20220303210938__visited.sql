CREATE SEQUENCE visited_id_seq;

CREATE TABLE visited
(
    id           integer NOT NULL DEFAULT nextval('visited_id_seq'),
    source_name  VARCHAR(200),
    source_url   VARCHAR(500),
    url          VARCHAR(500),
    last_scraped TIMESTAMP,
    PRIMARY KEY (id)
);