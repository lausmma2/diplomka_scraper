from twisted.internet import reactor, defer
from scrapy.crawler import CrawlerRunner
from scrapy.utils.log import configure_logging
from scrapy.utils.project import get_project_settings

from diplomka_scraper.spiders.timestamp_spider import TimestampSpider
from diplomka_scraper.spiders.head_spider import HeadSpider
from diplomka_scraper.spiders.head_xml_spider import HeadXMLSpider

configure_logging()
settings = get_project_settings()
runner = CrawlerRunner(settings)


@defer.inlineCallbacks
def crawl():
    # yield runner.crawl(HeadXMLSpider)
    # yield runner.crawl(HeadSpider)
    yield runner.crawl(TimestampSpider)
    yield runner.crawl(HeadSpider)
    reactor.stop()


crawl()
reactor.run()
