import json

import requests


def download_urls(site: str, fmt: str):
    url = "https://web.archive.org/cdx/search/cdx?url=%s&output=%s" % (site, fmt)
    response = requests.get(url)
    url_list: list = []
    for i in json.loads(response.text):
        url_list.append("https://web.archive.org/web/%s/%s" % (i[1], i[2]))

    return url_list
