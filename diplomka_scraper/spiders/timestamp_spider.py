from abc import ABC
from datetime import datetime
from urllib.parse import urlparse

import scrapy

from diplomka_scraper.items import HTMLTagItem
from ..download_urls import download_urls


class TimestampSpider(scrapy.Spider, ABC):
    name = 'timestamp_spider'
    custom_settings = {
        'ITEM_PIPELINES': {
            'diplomka_scraper.pipelines.VisitedPipeline': 300
        }
    }
    allowed_domains = ['web.archive.org']

    # start_urls = ['https://web.archive.org/cdx/search/cdx?url=bonprix.cz&output=json']

    sites: list = ['netagent.cz', 'bonprix.cz', 'ukazjakbydlis.cz', 'zoover.cz', 'jobdnes.cz', 'erento.cz',
                   'profesia.cz', 'tn.cz', 'auto.cz', 'miton.cz', 'blueboard.cz', 'alik.cz', 'centrum.cz',
                   'evzdelavani.cz', 'hifishop.cz', 'officedepot.cz', 'slando.cz', 'votocvohoz.cz', 'bilyshop.cz',
                   'klikni.cz', "port.cz", 'creative.cz', 'topwedding.cz', 'tuugo.cz', 'velkaepocha.cz',
                   'remax-czech.cz', 'choicehotels.cz', 'hays.cz', 'sgsgroup.cz', 'knightfrank.cz',
                   'lekari-ber-hranic.cz', 'citybase.cz', 'bmw.cz', 'asus.cz', 'vodafone.cz', 'restaurant-prague.com',
                   'olympijskytym.cz', 'pantharei.estranky.cz', 'ksc.cz', 'mcfc.cz',
                   'hausfargen.com', 'hotellapergola.com.cz', 'instyle-pergolas.com.au', 'pergolapatiocover.com',
                   'pergola-madera.es', 'pergola.pl', 'pergola-conseil.com', 'pergola.firenze.it',
                   'pergola-veranda.com', 'catering-pl.com', "harringtonscornedbeef.com", 'virtual-zoom.cz',
                   'thecateringjob.com', 'cateringrabanal.com', 'artursokol.pl', 'brixton-catering.cz',
                   'catering-menu.pl', 'catering.com', 'delikatering.cz', 'eleganscatering.com', 'zcater.com',
                   'cateringcanarias.com']

    fmt: str = "json"

    def __init__(self, **kwargs):
        super(TimestampSpider, self).__init__(**kwargs)
        self.DiplomkaScraperPipeline = None

    # Dynamic start urls
    def start_requests(self):
        for site in self.sites:
            for url in download_urls(site, self.fmt):
                yield scrapy.Request(url)

    def parse(self, response, **kwargs):

        item = HTMLTagItem()

        today = datetime.today()

        item["source_name"] = ""
        for site in self.sites:
            if site in response.url:
                item["source_name"] = site
        item["source_url"] = response.url
        item["url"] = response.url
        item["last_scraped"] = today

        yield item
