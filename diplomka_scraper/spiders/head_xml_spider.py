from abc import ABC
from datetime import datetime
from urllib.parse import urlparse

import scrapy
from scrapy.spiders import XMLFeedSpider
from diplomka_scraper.items import HTMLTagItem


class HeadXMLSpider(XMLFeedSpider, ABC):
    name = 'head_xml_spider'
    custom_settings = {
        'ITEM_PIPELINES': {
            'diplomka_scraper.pipelines.VisitedPipeline': 300
        }
    }
    # All allowed domains spider can scrape
    allowed_domains = [
        'mlj.solutions',
        'staveko.cz',
        'maron.cz',
        'bramburky.cz',
        'sunsystem.cz',
        'alukov.cz',
        'abrasiv.cz',
        'acomponents.cz',
        'agrometall.cz',
        'akkvb.cz',
        'animobohemia.cz',
        'bonafa.cz',
        'catering-bonte.cz',
        'derisol.cz',
        'dermaestetik.cz',
        'drevenekonstrukce.cz',
        'duha-uklid.cz',
        'ekocis.cz',
        'enerdomy.cz',
        'fitrevolution.cz',
        'glasspro.cz',
        'hasicskyservis.cz',
        'hoffmann.cz',
        'house-keeping.cz',
        'izolprotan.cz',
        'inste.cz',
        'kaplanpraha.cz',
        'karavanyplus.cz',
        'klinkercentrum.cz',
        'linaset.cz',
        'archive.org',
        'web.archive.org',
    ]

    live_webs: list = [
        "https://staveko.cz/sitemap.xml",
        "https://ebrana.cz/sitemap.xml",
        "https://www.maron.cz/sitemap.xml",
        "https://bramburky.cz/sitemap.xml",
        "https://alukov.cz/sitemap.xml",
        "https://sunsystem.cz/sitemap.xml",
        "https://mlj.solutions/sitemap.xml",
        "https://abrasiv.cz/sitemap.xml",
        "https://acomponents.cz/sitemap.xml",
        "https://agrometall.cz/sitemap.xml",
        "https://akkvb.cz/sitemap.xml",
        "https://animobohemia.cz/sitemap.xml",
        "https://bonafa.cz/sitemap.xml",
        "https://catering-bonte.cz/sitemap.xml",
        "https://derisol.cz/sitemap.xml",
        "https://dermaestetik.cz/sitemap.xml",
        "https://drevenekonstrukce.cz/sitemap.xml",
        "https://duha-uklid.cz/sitemap.xml",
        "https://ekocis.cz/sitemap.xml",
        "https://enerdomy.cz/sitemap.xml",
        "https://fitrevolution.cz/sitemap.xml",
        "https://glasspro.cz/sitemap.xml",
        "https://hasicskyservis.cz/sitemap.xml",
        "https://hoffmann.cz/sitemap.xml",
        "https://house-keeping.cz/sitemap.xml",
        "https://inste.cz/sitemap.xml",
        "https://izolprotan.cz/sitemap.xml",
        "https://kaplanpraha.cz/sitemap.xml",
        "https://karavanyplus.cz/sitemap.xml",
        "https://klinkercentrum.cz/sitemap.xml",
        "https://linaset.cz/sitemap.xml",
    ]
    # start_urls = ['https://www.maron.cz/sitemap.xml']
    iterator = 'html'  # This is actually unnecessary, since it's the default value
    itertag = 'url'

    """def start_requests(self):
        for url in self.live_webs:
            yield scrapy.Request(url)"""

    def start_requests(self):
        yield scrapy.Request("https://dermaestetik.cz/sitemap.xml")

    def parse_node(self, response, node):
        print("parse_node")
        self.logger.info('Hi, this is a <%s> node!: %s', self.itertag, ''.join(node.getall()))

        visited_item = HTMLTagItem()
        loc = node.xpath('loc/text()').get()

        today = datetime.today()
        source_name = urlparse(response.url).netloc

        visited_item['source_name'] = source_name
        visited_item['source_url'] = response.url
        visited_item['url'] = loc
        visited_item['last_scraped'] = today

        yield visited_item
