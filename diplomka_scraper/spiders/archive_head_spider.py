from datetime import datetime
from urllib.parse import urlparse

import scrapy

from diplomka_scraper.items import HTMLTagItem

from collections import Counter


class ArchiveHeadSpider(scrapy.Spider):
    """Spider for every URL"""
    name = "archive_headspider"

    custom_settings = {
        'ITEM_PIPELINES': {
            'diplomka_scraper.pipelines.HTMLTagPipeline': 300
        }
    }

    # All allowed domains spider can scrape
    allowed_domains = [
        'mlj.solutions',
        'staveko.cz',
        'maron.cz'
    ]

    def __init__(self, **kwargs):
        super(ArchiveHeadSpider, self).__init__(**kwargs)
        self.DiplomkaScraperPipeline = None

    # Dynamic start urls
    def start_requests(self):
        for url in self.DiplomkaScraperPipeline.get_visited_urls():
            yield scrapy.Request(url)

    # For every link call parse_section function
    def parse(self, response, **kwargs):
        print(response.url)

        html_tag_item = HTMLTagItem()

        title = response.css("head > title::text").get()
        description = response.xpath("//meta[@name='description']/@content").get(default=None)
        keywords = response.xpath("//meta[@name='keywords']/@content").get(default=None)
        charset = response.css("meta::attr(charset)").get(default=None)
        viewport = response.xpath("//meta[@name='viewport']/@content").get(default=None)
        # <style> and external css file
        stylesheet = response.xpath("//link[@rel='stylesheet']/@href").get(default=None)
        style = response.css("style::attr(data-href)").get(default=None)
        styles: list = [stylesheet, style]
        if None in styles: styles.remove(None)
        canonical = response.xpath("//link[@rel='canonical']/@href").get(default=None)
        robots = response.xpath("//meta[@name='robots']/@content").get(default=None)
        googlebot = response.xpath("//meta[@name='googlebot']/@content").get(default=None)
        generator = response.xpath("//meta[@name='generator']/@content").get(default=None)
        subject = response.xpath("//meta[@name='subject']/@content").get(default=None)
        rating = response.xpath("//meta[@name='rating']/@content").get(default=None)
        referrer = response.xpath("//meta[@name='referrer']/@content").get(default=None)
        amphtml = response.xpath("//link[@rel='amphtml']/@href").get(default=None)
        manifest = response.xpath("//link[@rel='manifest']/@href").get(default=None)
        author = response.xpath("//link[@rel='author']/@href").get(default=None)
        license_rel = response.xpath("//link[@rel='license']/@href").get(default=None)
        alternate = response.xpath("//link[@rel='alternate']/@href").get(default=None)
        index = response.xpath("//link[@rel='index']/@href").get(default=None)
        pingback = response.xpath("//link[@rel='pingback']/@href").get(default=None)
        webmention = response.xpath("//link[@rel='webmention']/@href").get(default=None)
        search = response.xpath("//link[@rel='search']/@href").get(default=None)
        icons_hrefs = response.xpath("//link[@rel='icon']/@href").get(default=None)
        icons_sizes = response.xpath("//link[@rel='icon']/@sizes").get(default=None)
        script = response.css("script::attr(src)").get(default=None)
        no_script = response.xpath("noscript").get(default=None)

        # TODO - scrape GA tag
        scripts = response.css("script::attr(src)").getall()
        apple_icons = response.xpath("//link[@rel='icon']/@sizes").getall()
        h1_titles = response.css("h1::text").getall()
        # Scrape all texts from <body> and find most occur words except stopwords
        # body = '\n'.join(response.xpath('//text()').getall())  # strip?
        body = '\n'.join(response.xpath('//body/descendant::text()[not(parent::script)]').extract())  # strip?
        stopwords = ["a", "na", "-", "and", "then", "you", "pro", "s", ",", ".", "jsou", "se", "o", "k", "{",
                     "Zobrazit", "zobrazit", "of", "the", "for", "in", "to", "717", "774", "940", "po", "do", "i",
                     "our", "with", "kdy", "jak", "proč", "kde", "kam", "v", "pre", "Jsme", "jsme", "my", "vy", "aby",
                     "abyste", "abychom", "bychom", "je", "si", "vám", "nebo", "za", "ale", "kraj", "jako"]
        splitted_body = body.split()
        body_without_stopwords = [word for word in splitted_body if word.lower() not in stopwords]
        counters_found = Counter(body_without_stopwords)
        most_used_words = counters_found.most_common(5)
        print(most_used_words)

        on_site_hrefs = response.xpath("//a/@href").extract()

        # Image urls and alts to jsonb
        images_array: list = []
        for image in response.xpath("//img"):
            image_url = image.attrib['src']
            image_alt = image.attrib['alt']
            image_dict = {
                'image': image_url,
                'alt': image_alt,
            }
            images_array.append(image_dict)

        today = datetime.today()
        source_name = urlparse(response.url).netloc

        html_tag_item['source_name'] = source_name
        html_tag_item['source_url'] = source_name + "/sitemap.xml"
        html_tag_item['url'] = response.url
        html_tag_item['title'] = title
        html_tag_item['description'] = description
        html_tag_item['keywords'] = keywords
        html_tag_item['charset'] = charset
        html_tag_item['viewport'] = viewport
        html_tag_item['styles'] = styles
        html_tag_item['canonical'] = canonical
        html_tag_item['robots'] = robots
        html_tag_item['googlebot'] = googlebot
        html_tag_item['generator'] = generator
        html_tag_item['subject'] = subject
        html_tag_item['rating'] = rating
        html_tag_item['referrer'] = referrer
        html_tag_item['amphtml'] = amphtml
        html_tag_item['manifest'] = manifest
        html_tag_item['author'] = author
        html_tag_item['license_rel'] = license_rel
        html_tag_item['alternate'] = alternate
        html_tag_item['index'] = index
        html_tag_item['pingback'] = pingback
        html_tag_item['webmention'] = webmention
        html_tag_item['search'] = search
        html_tag_item['icons_hrefs'] = icons_hrefs
        html_tag_item['icons_sizes'] = icons_sizes
        html_tag_item['script'] = script
        html_tag_item['no_script'] = no_script
        html_tag_item['scripts'] = scripts
        html_tag_item['apple_icons'] = apple_icons
        html_tag_item['h1_titles'] = h1_titles
        html_tag_item['body'] = splitted_body
        html_tag_item['most_used_words'] = most_used_words
        html_tag_item['on_site_hrefs'] = set(on_site_hrefs)
        html_tag_item['images'] = images_array

        has_multiple_css_files: bool = False
        if len(styles) > 1:
            has_multiple_css_files = True
        html_tag_item['has_multiple_css_files'] = has_multiple_css_files
        html_tag_item['css_files_count'] = len(styles)

        html_tag_item['last_scraped'] = today

        yield html_tag_item
