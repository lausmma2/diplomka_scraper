from datetime import datetime
from urllib.parse import urlparse

import scrapy
from unidecode import unidecode

from diplomka_scraper.items import HTMLTagItem

from collections import Counter


class HeadSpider(scrapy.Spider):
    """Spider for every URL"""
    name = "headspider"

    custom_settings = {
        'ITEM_PIPELINES': {
            'diplomka_scraper.pipelines.HTMLTagPipeline': 300
        }
    }

    # All allowed domains spider can scrape
    allowed_domains = [
        'archive.org',
        'web.archive.org',
    ]

    def __init__(self, **kwargs):
        super(HeadSpider, self).__init__(**kwargs)
        self.DiplomkaScraperPipeline = None

    # Dynamic start urls
    def start_requests(self):
        for url in self.DiplomkaScraperPipeline.get_visited_urls():
            yield scrapy.Request(url)

    # For every link call parse_section function
    def parse(self, response, **kwargs):
        print(response.url)

        html_tag_item = HTMLTagItem()

        html_lang = response.xpath("//html/body/iframe/html/@lang").get(default="")
        html_xml_lang = response.xpath("//html/body/iframe/html/@xml:lang").get(default="")
        html_langs = html_lang + html_xml_lang
        title = response.css("head > title::text").get(default="")
        meta_title_2 = response.xpath("//meta[@name='Title']/@content").get(default="")
        meta_title = response.xpath("//meta[@name='title']/@content").get(default="")
        description_2 = response.xpath("//meta[@name='Description']/@content").get(default="")
        description = response.xpath("//meta[@name='description']/@content").get(default="")
        keywords_2 = response.xpath("//meta[@name='Keywords']/@content").get(default="")
        keywords = response.xpath("//meta[@name='keywords']/@content").get(default="")
        charset = response.css("meta::attr(charset)").get(default="")
        viewport = response.xpath("//meta[@name='viewport']/@content").get(default="")
        # <style> and external css file
        stylesheet = response.xpath("//link[@rel='stylesheet']/@href").get(default="")
        style = response.css("style::attr(data-href)").get(default="")
        styles: list = [stylesheet, style]
        if None in styles: styles.remove(None)
        canonical = response.xpath("//link[@rel='canonical']/@href").get(default="")
        robots = response.xpath("//meta[@name='robots']/@content").get(default="")
        googlebot = response.xpath("//meta[@name='googlebot']/@content").get(default="")
        generator = response.xpath("//meta[@name='generator']/@content").get(default="")
        subject = response.xpath("//meta[@name='subject']/@content").get(default="")
        rating = response.xpath("//meta[@name='rating']/@content").get(default="")
        referrer = response.xpath("//meta[@name='referrer']/@content").get(default="")
        amphtml = response.xpath("//link[@rel='amphtml']/@href").get(default="")
        manifest = response.xpath("//link[@rel='manifest']/@href").get(default="")
        author = response.xpath("//link[@rel='author']/@content").get(default="")
        license_rel = response.xpath("//link[@rel='license']/@href").get(default="")
        alternate = response.xpath("//link[@rel='alternate']/@href").getall()
        index = response.xpath("//link[@rel='index']/@href").get(default="")
        pingback = response.xpath("//link[@rel='pingback']/@href").get(default="")
        webmention = response.xpath("//link[@rel='webmention']/@href").get(default="")
        search = response.xpath("//link[@rel='search']/@href").get(default="")
        icons_hrefs = response.xpath("//link[@rel='icon']/@href").get(default="")
        icons_sizes = response.xpath("//link[@rel='icon']/@sizes").get(default="")
        script = response.css("script::attr(src)").get(default="")
        no_script = response.xpath("noscript").get(default="")

        header = response.xpath("header").get(default="")
        footer = response.xpath("footer").get(default="")
        main = response.xpath("main").get(default="")
        div_header = response.xpath("//div[@id='header']").get(default="")
        div_footer = response.xpath("//div[@id='footer']").get(default="")
        div_main = response.xpath("//div[@id='main']").get(default="")
        has_header = False
        has_footer = False
        has_main = False
        if header is not None or div_header is not None or header != "" or div_header != "":
            has_header = True
        if footer is not None or div_footer is not None or footer != "" or div_footer != "":
            has_footer = True
        if main is not None or div_main is not None or main != "" or div_main != "":
            has_main = True

        scripts = response.css("script::attr(src)").getall()
        apple_icons = response.xpath("//link[@rel='icon']/@sizes").getall()
        h1_titles = response.css("h1::text").extract()
        body = '\n'.join(
            response.xpath('//body/descendant::text()[not(parent::script)][not(parent::style)]').extract())  # strip?
        stopwords = ["a", "na", "-", "and", "then", "you", "pro", "s", ",", ".", "jsou", "se", "o", "k", "{", "_", "–",
                     "Zobrazit", "zobrazit", "of", "the", "for", "in", "to", "717", "774", "940", "po", "do", "i", "A",
                     "our", "with", "kdy", "jak", "proč", "kde", "kam", "v", "pre", "Jsme", "jsme", "my", "vy", "aby",
                     "abyste", "abychom", "bychom", "je", "si", "vám", "nebo", "za", "ale", "kraj", "jako", "od", "Od",
                     "!important;", "Alexa", "data", "Crawls", "Internet", "Wayback", "\uee00", "\uf610", "\uf611",
                     "\ue399", "are", "as", "is", "on", "that", "this", "me", "you", "he", "she", "it", "we", "they",
                     "Are", "As", "Is", "On", "That", "This", "Me", "You", "He", "She", "It", "We", "They", "Of", "or",
                     "Or", "A", "The", "the", "hours", "Hours", "✉", "|", "-", "u", "až", "pak", "potom", "v", "z",
                     "Kč", "kč", "An", "cookies", "Cookies", "osobních", "údajů", "*", "nám", "vám", "Nám", "Vám",
                     "vás", "nás", "Napište", "napište", "s.r.o.", "web", "které", "který", "Které", "Který", "proti",
                     "mít", "Mít", "ODKAZ", "Odkaz", "odkaz", "bez", "Bez", "wayback", "x", "Když", "dva", "tak", "Tak",
                     "mm", "napríklad", "už", "jen", "Souhlasím", "našich", "tento", "Tento", "Nastavení", "+420",
                     "jste", "Jste", "při", "Při", "jejich", "Jejich", "jim", "Jim", "ve", "že", "Vše", "vše", "více",
                     "umožňují", "©", "Copyright", "copyright", "Developed", "by", "co", "chci", "není", "je", "či",
                     "an", "also", "can", "two", "...", "včera", "08.", "2005", "07.", "Váš", "můžete", "podle",
                     "další",
                     "zde...", "vašeho", "zde", "0", "1", "Machine", "jsem", "7:30", "4:20"]
        splitted_body = body.split()
        body_without_stopwords = [word for word in splitted_body[97:] if word.lower() not in stopwords]
        counters_found = Counter(body_without_stopwords)
        most_used_words = counters_found.most_common(10)
        print(most_used_words)

        temp: int = 0
        if keywords is not None:
            keywords_array = keywords.split(",")
            for keyword in keywords_array:
                if keyword.lower() in body_without_stopwords:
                    temp += 1
        elif keywords_2 is not None:
            keywords_2_array = keywords_2.split(",")
            for keyword in keywords_2_array:
                if keyword.lower() in body_without_stopwords:
                    temp += 1

        on_site_hrefs = response.xpath("//a/@href").extract()

        # Image urls and alts to jsonb
        images_array: list = []
        images = response.xpath("//img")

        alts_counter: int = 0
        if images is not None:
            for image in images:
                image_url = image.css('img::attr(src)').get(default="")
                image_alt = image.css('img::attr(alt)').get(default="")
                if "wayback" in image_url:
                    continue
                if "Wayback" in image_alt:
                    image_alt = ""
                image_dict = {
                    'image': image_url,
                    'alt': image_alt,
                }
                if image_alt != "":
                    alts_counter += 1
                images_array.append(image_dict)

        # Check for duplicities in <title> tag
        has_title_duplicities: bool = False
        titles = title + meta_title + meta_title_2
        splitted_titles = titles.split()
        for word in splitted_titles:
            visited = set()
            word_lower = word.lower()
            if word_lower in visited:
                has_title_duplicities = True
            else:
                visited.add(word_lower)

        # Convert elements in splitted_titles to lowercase and unidecode them
        edited_splitted_titles: list = []
        for word in splitted_titles:
            word_lower = word.lower()
            word_edited = unidecode(word_lower)
            edited_splitted_titles.append(word_edited)

        # Check if most used keywords in text occur in <title> tag
        has_title_keywords: bool = False
        """for word in most_used_words:
            edited_word = unidecode(word[0])
            edited_word_lower = edited_word.lower()
            if edited_word_lower in edited_splitted_titles:
                has_title_keywords = True"""

        keywords_joined = keywords + keywords_2
        keywords_splitted = keywords_joined.split()
        for k in keywords_splitted:
            if k in splitted_titles:
                has_title_keywords = True

        has_description_keywords: bool = False
        descriptions_joined = keywords + keywords_2
        descriptions_splitted = descriptions_joined.split()
        for k in keywords_splitted:
            if k in descriptions_splitted:
                has_description_keywords = True

        today = datetime.today()
        source_name = urlparse(response.url).netloc

        html_tag_item['source_name'] = source_name
        html_tag_item['source_url'] = source_name + "/sitemap.xml"
        html_tag_item['url'] = response.url
        url_timestamp = response.url.split("web/")[1]
        parsed_url_timestamp = datetime.strptime(url_timestamp[:14], "%Y%m%d%H%M%S")
        html_tag_item['url_timestamp'] = parsed_url_timestamp  # "10-04-19 12:00:17"
        html_tag_item['html_lang'] = html_langs
        if title is not None:
            html_tag_item['title'] = title.strip()  # 55-65 chars
        else:
            html_tag_item['title'] = title  # 55-65 chars
        html_tag_item['title_length'] = len(title)
        html_tag_item['meta_title_2'] = meta_title_2  # 55-65 chars
        html_tag_item['meta_title_2_length'] = len(meta_title_2) if meta_title_2 is not None else 0
        html_tag_item['meta_title'] = meta_title  # 55-65 chars
        html_tag_item['meta_title_length'] = len(meta_title) if meta_title is not None else 0
        html_tag_item['description_2'] = description_2  # 150-160 chars
        html_tag_item['description_2_length'] = len(description_2) if description_2 is not None else 0
        html_tag_item['description'] = description  # 150-160 chars
        html_tag_item['description_length'] = len(description) if description is not None else 0
        html_tag_item['keywords_2'] = keywords_2
        html_tag_item['keywords'] = keywords
        html_tag_item['charset'] = charset
        html_tag_item['viewport'] = viewport
        html_tag_item['styles'] = styles
        html_tag_item['canonical'] = canonical
        html_tag_item['robots'] = robots
        html_tag_item['googlebot'] = googlebot
        html_tag_item['generator'] = generator
        html_tag_item['subject'] = subject
        html_tag_item['rating'] = rating
        html_tag_item['referrer'] = referrer
        html_tag_item['amphtml'] = amphtml
        html_tag_item['manifest'] = manifest
        html_tag_item['author'] = author
        html_tag_item['license_rel'] = license_rel
        html_tag_item['alternate'] = alternate
        html_tag_item['index'] = index
        html_tag_item['pingback'] = pingback
        html_tag_item['webmention'] = webmention
        html_tag_item['search'] = search
        html_tag_item['icons_hrefs'] = icons_hrefs
        html_tag_item['icons_sizes'] = icons_sizes
        html_tag_item['script'] = script
        html_tag_item['no_script'] = no_script
        html_tag_item['scripts'] = scripts
        html_tag_item['apple_icons'] = apple_icons
        html_tag_item['h1_titles'] = h1_titles
        html_tag_item['has_more_h1'] = False
        html_tag_item['h1_length'] = 0
        if len(h1_titles) > 1:
            html_tag_item['has_more_h1'] = True
            html_tag_item['h1_length'] = len(h1_titles)
        html_tag_item['body'] = splitted_body[97:]
        chars_count = 0
        for word in body_without_stopwords:
            chars_count += len(word)
        html_tag_item['chars_count'] = chars_count
        html_tag_item['words_count'] = len(body_without_stopwords)
        # how many % are keywords from the whole text body
        if len(body_without_stopwords) != 0:
            html_tag_item['keywords_percent'] = (temp / len(body_without_stopwords)) * 100
        else:
            html_tag_item['keywords_percent'] = 0
        html_tag_item['most_used_words'] = most_used_words
        html_tag_item['on_site_hrefs'] = set(on_site_hrefs)
        html_tag_item['images'] = images_array
        html_tag_item['images_count'] = len(images_array)
        html_tag_item['alts_count'] = alts_counter

        has_multiple_css_files: bool = False
        if len(styles) > 1:
            has_multiple_css_files = True
        html_tag_item['has_multiple_css_files'] = has_multiple_css_files
        html_tag_item['css_files_count'] = len(styles)

        html_tag_item['has_header'] = has_header
        html_tag_item['has_footer'] = has_footer
        html_tag_item['has_main'] = has_main
        html_tag_item['header'] = header
        html_tag_item['footer'] = footer
        html_tag_item['main'] = main
        html_tag_item['div_header'] = div_header
        html_tag_item['div_footer'] = div_footer
        html_tag_item['div_main'] = div_main

        html_tag_item['has_title_keywords'] = has_title_keywords
        html_tag_item['has_description_keywords'] = has_description_keywords
        html_tag_item['has_title_duplicities'] = has_title_duplicities

        html_tag_item['last_scraped'] = today

        yield html_tag_item
