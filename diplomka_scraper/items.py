import scrapy


class HTMLTagItem(scrapy.Item):
    source_name = scrapy.Field()
    source_url = scrapy.Field()
    url = scrapy.Field()

    html_lang = scrapy.Field()
    title = scrapy.Field()
    title_length = scrapy.Field()
    meta_title_2 = scrapy.Field()
    meta_title_2_length = scrapy.Field()
    meta_title = scrapy.Field()
    meta_title_length = scrapy.Field()
    description_2 = scrapy.Field()
    description_2_length = scrapy.Field()
    description = scrapy.Field()
    description_length = scrapy.Field()
    keywords_2 = scrapy.Field()
    keywords = scrapy.Field()
    charset = scrapy.Field()
    viewport = scrapy.Field()
    styles = scrapy.Field()
    canonical = scrapy.Field()
    robots = scrapy.Field()
    googlebot = scrapy.Field()
    generator = scrapy.Field()
    subject = scrapy.Field()
    rating = scrapy.Field()
    referrer = scrapy.Field()
    amphtml = scrapy.Field()
    manifest = scrapy.Field()
    author = scrapy.Field()
    license_rel = scrapy.Field()
    alternate = scrapy.Field()
    index = scrapy.Field()
    pingback = scrapy.Field()
    webmention = scrapy.Field()
    search = scrapy.Field()
    icons_hrefs = scrapy.Field()
    icons_sizes = scrapy.Field()
    script = scrapy.Field()
    no_script = scrapy.Field()
    scripts = scrapy.Field()
    apple_icons = scrapy.Field()
    h1_titles = scrapy.Field()
    has_more_h1 = scrapy.Field()
    h1_length = scrapy.Field()
    body = scrapy.Field()
    chars_count = scrapy.Field()
    words_count = scrapy.Field()
    keywords_percent = scrapy.Field()
    most_used_words = scrapy.Field()
    on_site_hrefs = scrapy.Field()
    images = scrapy.Field()
    images_count = scrapy.Field()
    alts_count = scrapy.Field()
    has_multiple_css_files = scrapy.Field()
    css_files_count = scrapy.Field()

    has_header = scrapy.Field()
    has_footer = scrapy.Field()
    has_main = scrapy.Field()
    header = scrapy.Field()
    footer = scrapy.Field()
    main = scrapy.Field()
    div_header = scrapy.Field()
    div_footer = scrapy.Field()
    div_main = scrapy.Field()

    url_timestamp = scrapy.Field()

    has_title_keywords = scrapy.Field()
    has_description_keywords = scrapy.Field()
    has_title_duplicities = scrapy.Field()

    last_scraped = scrapy.Field()
