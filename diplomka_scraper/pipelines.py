import json
from typing import Optional, List

import psycopg2
import logging
from psycopg2.extensions import connection as Connection
from scrapy.utils.project import get_project_settings
from .items import HTMLTagItem

# Connection to database
from .spiders.head_spider import HeadSpider
from .spiders.head_xml_spider import HeadXMLSpider
from .spiders.timestamp_spider import TimestampSpider


def create_connection_to_db(host: str, user: str, password: str, port: int, database: str) -> Connection:
    logging.info("CREATE_CONNECTION_TO_DB")
    logging.info(get_project_settings())

    return psycopg2.connect(
        host=host,
        user=user,
        password=password,
        port=port,
        database=database
    )


class VisitedPipeline:
    visited_urls: Optional[List[str]] = None
    html_tags_urls: Optional[List[str]] = None
    connection: Connection = None

    def __init__(self, conn: Connection):
        self.connection = conn

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            conn=create_connection_to_db(
                host="127.0.0.1",
                port=5432,
                user="postgres",
                password="postgres",
                database="diplomka_scraper",
            )
        )

    def store_url_to_db_execute(self, item: HTMLTagItem) -> None:
        logging.debug("STORE_URL_TO_DB_EXECUTE")
        pg_insert = """INSERT INTO visited(source_name, source_url, url, last_scraped) VALUES(%s, %s, %s, %s)"""
        inserted_value = (item['source_name'], item['source_url'], item['url'], item['last_scraped'])
        try:
            with self.connection.cursor() as curs:
                curs.execute(pg_insert, inserted_value)
            self.connection.commit()
        except Exception as e:
            logging.fatal(e)
            self.connection.rollback()

    # Check and return stored URLs
    def get_visited_urls(self):
        if self.visited_urls is not None:
            return self.visited_urls

        # Select all URLs from database
        postgresql_select_query = "SELECT url FROM visited"
        with self.connection.cursor() as curs:
            curs.execute(postgresql_select_query)
            self.visited_urls: Optional[List[str]] = []
            for url in curs.fetchall():  # fetchall() always fetches data as a tuple
                self.visited_urls.append(url[0])

        return self.get_visited_urls()

    def process_item(self, item: HTMLTagItem, spider: HeadXMLSpider):
        logging.debug("HeadXMLSpider_PROCESS_ITEM")

        if item['url'] not in self.get_visited_urls():
            self.store_url_to_db_execute(item)
            self.visited_urls.append(item['url'])

        return item

    # Pipeline instance creation in spider
    def open_spider(self, spider: HeadXMLSpider) -> None:
        spider.DiplomkaScraperPipeline = self

    def close_spider(self, spider: HeadXMLSpider):
        self.connection.close()


class HTMLTagPipeline:
    visited_urls: Optional[List[str]] = None
    last_visited_urls: Optional[List[str]] = None
    html_tags_urls: Optional[List[str]] = None
    connection: Connection = None

    def __init__(self, conn: Connection):
        self.connection = conn

    @classmethod
    def from_crawler(cls, crawler):
        return cls(
            conn=create_connection_to_db(
                host="127.0.0.1",
                port=5432,
                user="postgres",
                password="postgres",
                database="diplomka_scraper",
            )
        )

    def store_url_to_db_execute(self, item: HTMLTagItem) -> None:
        logging.debug("STORE_URL_TO_DB_EXECUTE")
        pg_insert = """INSERT INTO visited(source_name, source_url, url, last_scraped) VALUES(%s, %s, %s, %s)"""
        inserted_value = (item['source_name'], item['source_url'], item['url'], item['last_scraped'])
        try:
            with self.connection.cursor() as curs:
                curs.execute(pg_insert, inserted_value)
            self.connection.commit()
        except Exception as e:
            logging.fatal(e)
            self.connection.rollback()

    # Check and return stored URLs
    def get_visited_urls(self):
        if self.visited_urls is not None:
            return self.visited_urls

        # Select all URLs from database
        postgresql_select_query = "SELECT url FROM visited"
        with self.connection.cursor() as curs:
            curs.execute(postgresql_select_query)
            self.visited_urls: Optional[List[str]] = []
            for url in curs.fetchall():  # fetchall() always fetches data as a tuple
                self.visited_urls.append(url[0])

        return self.get_visited_urls()

    # Check and return stored URLs
    def get_last_visited_urls(self):
        if self.last_visited_urls is not None:
            return self.last_visited_urls

        # Select all URLs from database
        postgresql_select_query = "SELECT url FROM visited WHERE id > 39064"
        with self.connection.cursor() as curs:
            curs.execute(postgresql_select_query)
            self.last_visited_urls: Optional[List[str]] = []
            for url in curs.fetchall():  # fetchall() always fetches data as a tuple
                self.last_visited_urls.append(url[0])

        return self.get_last_visited_urls()

    # Check and return stored URLs
    def get_html_tags_urls(self):
        if self.html_tags_urls is not None:
            return self.html_tags_urls

        # Select all URLs from database
        postgresql_select_query = "SELECT url FROM html_tag"
        with self.connection.cursor() as curs:
            curs.execute(postgresql_select_query)
            self.html_tags_urls: Optional[List[str]] = []
            for url in curs.fetchall():  # fetchall() always fetches data as a tuple
                self.html_tags_urls.append(url[0])

        return self.get_html_tags_urls()

    def store_html_tags_to_db_execute(self, item: HTMLTagItem) -> None:
        logging.debug("STORE_HTML_TAGS_TO_DB_EXECUTE")
        pg_insert = """INSERT INTO html_tag(source_name, source_url, url, url_timestamp, html_lang, title, title_length, 
        meta_title_2, meta_title_2_length, description_2, description_2_length, keywords_2, meta_title, meta_title_length, 
        description, description_length, keywords, charset, viewport, styles, canonical, robots, googlebot, generator, 
        subject, rating, referrer, amphtml, manifest, author, license_rel, alternate, index, pingback, webmention, 
        search, icons_hrefs, icons_sizes, script, no_script, scripts, apple_icons, h1_titles, has_more_h1, h1_length, body, 
        chars_count, words_count, keywords_percent, most_used_words, on_site_hrefs, images, images_count, alts_count,
        has_multiple_css_files, css_files_count, has_header, has_footer, has_main, header, footer, main, div_header, 
        div_footer, div_main, has_title_keywords, has_description_keywords, has_title_duplicities, last_scraped) 
        VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
         %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
        inserted_value = (
            item['source_name'], item['source_url'], item['url'], item['url_timestamp'], item['html_lang'],
            item['title'], item['title_length'], item['meta_title_2'], item['meta_title_2_length'],
            item['description_2'], item['description_2_length'], item['keywords_2'], item['meta_title'],
            item['meta_title_length'], item['description'], item['description_length'], item['keywords'],
            item['charset'], item['viewport'], item['styles'], item['canonical'],
            item['robots'], item['googlebot'], item['generator'], item['subject'], item['rating'],
            item['referrer'], item['amphtml'], item['manifest'], item['author'], item['license_rel'],
            item['alternate'], item['index'], item['pingback'], item['webmention'], item['search'],
            item['icons_hrefs'], item['icons_sizes'], item['script'], item['no_script'], item['scripts'],
            item['apple_icons'], item['h1_titles'], item['has_more_h1'], item['h1_length'], item['body'],
            item['chars_count'], item['words_count'], item['keywords_percent'], item['most_used_words'],
            list(item['on_site_hrefs']), json.dumps(item['images']), item['images_count'], item['alts_count'],
            item['has_multiple_css_files'], item['css_files_count'], item['has_header'], item['has_footer'],
            item['has_main'], item['header'], item['footer'], item['main'], item['div_header'], item['div_footer'],
            item['div_main'], item['has_title_keywords'], item['has_description_keywords'],
            item['has_title_duplicities'], item['last_scraped'])
        try:
            with self.connection.cursor() as curs:
                curs.execute(pg_insert, inserted_value)
            self.connection.commit()
        except Exception as e:
            logging.fatal(e)
            self.connection.rollback()

    def process_item(self, item: HTMLTagItem, spider: HeadSpider):
        logging.debug("HeadSpider_PROCESS_ITEM")

        if item['url'] not in self.get_html_tags_urls():
            self.store_html_tags_to_db_execute(item)
            self.html_tags_urls.append(item['url'])
        elif item['url'] in self.get_html_tags_urls():
            print("...Dropping Item...")

        return item

    # Pipeline instance creation in spider
    def open_spider(self, spider: HeadSpider) -> None:
        spider.DiplomkaScraperPipeline = self

    def close_spider(self, spider: HeadSpider):
        self.connection.close()
