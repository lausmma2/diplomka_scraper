# Include environment variables if possible
-include .env
#-include .env.production
TIME := $(shell date +"%Y%m%d%H%M%S")

all: install test build

install:
	pip install -r requirements.txt
	flyway > /dev/null && echo "OK" || echo "Install FLYWAY from https://flywaydb.org/download/community"

migrate:
	flyway -url=${FLYWAY_URL} -password=${FLYWAY_PASSWORD} -user=${FLYWAY_USER} -locations=migrations -outOfOrder=true migrate

migrate-down:
	flyway -url=${FLYWAY_URL} -password=${FLYWAY_PASSWORD} -user=${FLYWAY_USER} -locations=migrations undo

migrate-drop:
	flyway -url=${FLYWAY_URL} -password=${FLYWAY_PASSWORD} -user=${FLYWAY_USER} -locations=migrations clean

migrate-status:
	flyway -url=${FLYWAY_URL} -password=${FLYWAY_PASSWORD} -user=${FLYWAY_USER} -locations=migrations info

migrate-reset: migrate-drop migrate

migrate-create:
	touch migrations/V${TIME}__RENAME.sql
	touch migrations/U${TIME}__RENAME.sql

.PHONY: test